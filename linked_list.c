#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "linked_list.h"

bool addToLinkList(struct linkedList *ll, int value)
{
    struct node *newNode = malloc(sizeof(struct node));
    if (newNode == NULL)
    {
        printf("Error on allocating node.\n");
        return false;
    }
    newNode->value = value;
    if (ll->start == NULL)
    {
        ll->start = newNode;
        ll->start->next = newNode;
        return true;
    }
    else
    {
        struct node *lastNode = getLastNode(ll);
        lastNode->next = newNode;
        newNode->next = ll->start;
        return true;
    }
    return false;
}

bool removeFromLinkListByValue(struct linkedList *ll, int value)
{
    if (ll->start == NULL)
    {
        // List is empty
        return false;
    }
    if (ll->start->value == value)
    {
        // Remove first node
        struct node *lastNode = getLastNode(ll);
        ll->start = ll->start->next;
        lastNode->next = ll->start;
        return true;
    }
    struct node *temp = ll->start;
    struct node *cursor = ll->start->next;
    while (cursor != ll->start)
    {
        if (value == cursor->value)
        {
            temp->next = cursor->next;
            return true;
        }
        else
        {
            temp = cursor;
            cursor = cursor->next;
        }
    }
    return false;
}

void traverse(struct linkedList *ll)
{
    if (ll->start == NULL)
    {
        printf("List is Empty\n");
        return;
    }
    struct node *cursor = ll->start;
    while (cursor->next != ll->start)
    {
        printf("Reading entry: %d\n", cursor->value);
        cursor = cursor->next;
    }
    printf("Reading entry: %d\n", cursor->value);
}

int getCountOfLinkList(struct linkedList *ll)
{
    if (ll->start == NULL)
    {
        return 0;
    }
    struct node *cursor = ll->start;
    int i = 1;
    while (cursor->next != ll->start)
    {
        i++;
        cursor = cursor->next;
    }
    return i;
}

struct node *getLastNode(struct linkedList *ll)
{
    struct node *cursor = ll->start;
    while (cursor->next != ll->start)
    {
        cursor = cursor->next;
    }
    return cursor;
}