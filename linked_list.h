struct node
{
    int value;
    struct node *next;
};

struct linkedList
{
    struct node *start;
};

bool addToLinkList(struct linkedList *ll, int value);
int getCountOfLinkList(struct linkedList *ll);
void traverse(struct linkedList *ll);
bool removeFromLinkListByValue(struct linkedList *ll, int value);
struct node *getLastNode(struct linkedList *ll);