#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "linked_list.h"

int main(void)
{
    srand(time(NULL));
    struct linkedList *ll = (struct linkedList *)malloc(sizeof(struct linkedList));
    for (int i = 1; i <= 10000; i++)
    {
        if (addToLinkList(ll, i))
            printf("Adding %d\n", i);
    }
    traverse(ll);
    for (int i = 1; i <= 100; i++) {
        int number = rand() % 10000 + 1;
        if (removeFromLinkListByValue(ll, number))
            printf("Deleting %d\n", number);
    }
}