# OS Fall 18: HW1
## LinkedList

Implemented methods:

| Method name               | Output type | Parameters                |
| ------------------------- | ----------- | ------------------------- |
| addToLinkList             | bool        | linkedList *ll, int value |
| getCountOfLinkList        | void        | linkedList *ll            |
| traverse                  | void        | linkedList *ll            |
| removeFromLinkListByValue | bool        | linkedList *ll, int value |
| getLastNode               | node*       | linkedList *ll            |


## How to run code

```bash
$ gcc main.c linked_list.c
$ ./a.out
```

## How to test code
You can test code by changing 'main.c' file.
the current test is:
+ Adding 1 to 10000 to the list
+ Traverse list show items
+ Delete 100 random numbers from list